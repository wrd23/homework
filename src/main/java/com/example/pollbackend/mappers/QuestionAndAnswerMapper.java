package com.example.pollbackend.mappers;

import com.example.pollbackend.dtos.AnswerDto;
import com.example.pollbackend.dtos.QuestionDto;
import com.example.pollbackend.entities.Answer;
import com.example.pollbackend.entities.Question;

import java.util.List;
import java.util.stream.Collectors;

public class QuestionAndAnswerMapper {

    public Question mapQuestionDtoToEntity(QuestionDto questionDto) {
        return Question.builder()
            .answerType(questionDto.getAnswerType())
            .answers(mapAnswers(questionDto.getAnswerDtoList()))
            .text(questionDto.getQuestion())
            .build();
    }

    public QuestionDto mapQuestionEntityToDto(Question question) {
        return QuestionDto.builder()
            .answerType(question.getAnswerType())
            .answerDtoList(mapAnswerDtos(question.getAnswers()))
            .question(question.getText())
            .build();
    }

    public Answer mapAnswerDtoToEntity(AnswerDto answerDto) {
        return Answer.builder()
            .text(answerDto.getText())
            .count(answerDto.getCount())
            .build();
    }

    public AnswerDto mapAnswerEntityToDto(Answer answer) {
        return AnswerDto.builder()
            .text(answer.getText())
            .count(answer.getCount())
            .build();
    }

    public List<Answer> mapAnswers(List<AnswerDto> answerDtoList) {
        return answerDtoList.stream()
            .map(this::mapAnswerDtoToEntity)
            .collect(Collectors.toList());
    }

    public List<AnswerDto> mapAnswerDtos(List<Answer> answers) {
        return answers.stream()
            .map(this::mapAnswerEntityToDto)
            .collect(Collectors.toList());
    }

    public List<QuestionDto> mapQuestionDtos(List<Question> questions) {
        return questions.stream()
            .map(this::mapQuestionEntityToDto)
            .collect(Collectors.toList());
    }
}

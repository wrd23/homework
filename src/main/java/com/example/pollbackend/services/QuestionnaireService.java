package com.example.pollbackend.services;

import com.example.pollbackend.dtos.AnswerDto;
import com.example.pollbackend.dtos.QuestionDto;
import com.example.pollbackend.dtos.QuestionnaireDto;
import com.example.pollbackend.entities.Answer;
import com.example.pollbackend.entities.Question;
import com.example.pollbackend.mappers.QuestionAndAnswerMapper;
import com.example.pollbackend.repositories.AnswerRepository;
import com.example.pollbackend.repositories.QuestionRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class QuestionnaireService {

    private final QuestionRepository questionRepository;
    private final AnswerRepository answerRepository;
    private final QuestionAndAnswerMapper questionAndAnswerMapper;

    public void saveAllAnswers(List<AnswerDto> answerDtoList) {

        Map<Long, List<AnswerDto>> answersByQuestionId =
            answerDtoList
                .stream()
                .collect(Collectors.groupingBy(AnswerDto::getQuestionId));

        answersByQuestionId.forEach((key, value) -> {
            Question question =
                questionRepository.findById(key)
                    .orElseThrow(() -> new NoSuchElementException(
                        "Question with the ID " + key + "not found!"));
            List<Answer> answers = questionAndAnswerMapper.mapAnswers(value);
            answers.forEach(answer -> {
                answer.setQuestion(question);
                answerRepository.save(answer);
            });
            question.setAnswers(answers);
            questionRepository.save(question);
        });
    }

    public QuestionnaireDto getQuestionnaire() {

        List<Question> questions = questionRepository.findAll();
        List<QuestionDto> questionDtoList = questionAndAnswerMapper.mapQuestionDtos(questions);
        return QuestionnaireDto.builder()
            .questionDtoList(questionDtoList)
            .build();
    }

    public QuestionDto getAnswersByQuestion(Long questionId) {

        Question question = questionRepository.findById(questionId)
            .orElseThrow(() -> new NoSuchElementException(
                "Question with the ID " + questionId + "not found!"));

        return questionAndAnswerMapper.mapQuestionEntityToDto(question);
    }
}

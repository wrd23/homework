package com.example.pollbackend.enums;

public enum AnswerType {
    SELECT_ONE,
    SELECT_MORE,
    FREE_TEXT
}

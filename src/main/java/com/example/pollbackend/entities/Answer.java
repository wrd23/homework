package com.example.pollbackend.entities;

import jakarta.persistence.*;
import lombok.*;

@Entity
@Table(name = "answer")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class Answer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "text", columnDefinition = "TEXT")
    private String text;

    @Column(name = "count", columnDefinition = "NUMBER")
    private Long count;

    @ManyToOne(targetEntity = Question.class)
    private Question question;
}

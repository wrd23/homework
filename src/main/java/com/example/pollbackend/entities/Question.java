package com.example.pollbackend.entities;

import com.example.pollbackend.enums.AnswerType;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.Cascade;

import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "question")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class Question {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "text", columnDefinition = "TEXT")
    private String text;

    @Enumerated(EnumType.STRING)
    @Column(name = "answer_type")
    private AnswerType answerType;

    @OneToMany(targetEntity = Answer.class, fetch = FetchType.EAGER, mappedBy = "answer")
    @Column(name = "answers")
    private List<Answer> answers = new ArrayList<>();
}

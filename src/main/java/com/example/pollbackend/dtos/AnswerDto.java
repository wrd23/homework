package com.example.pollbackend.dtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

@Getter
@AllArgsConstructor
@Builder
public class AnswerDto {

    private final long questionId;
    private final String text;
    private final long count;
}

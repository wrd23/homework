package com.example.pollbackend.dtos;

import com.example.pollbackend.enums.AnswerType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import java.util.List;

@Getter
@AllArgsConstructor
@Builder
public class QuestionDto {

    private final String question;
    private final AnswerType answerType;
    private final List<AnswerDto> answerDtoList;
}

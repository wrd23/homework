package com.example.pollbackend.dtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import java.util.List;

@Getter
@AllArgsConstructor
@Builder
public class QuestionnaireDto {

    private final List<QuestionDto> questionDtoList;
}

package com.example.pollbackend.controllers;

import com.example.pollbackend.dtos.AnswerDto;
import com.example.pollbackend.dtos.QuestionDto;
import com.example.pollbackend.dtos.QuestionnaireDto;
import com.example.pollbackend.services.QuestionnaireService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
@Slf4j
@RequestMapping("/api")
public class QuestionnaireController {

    private final QuestionnaireService questionnaireService;

    @GetMapping("/questionnaire")
    public ResponseEntity<QuestionnaireDto> getQuestionnaire() {

        log.info("Loading questionnaire...");
        QuestionnaireDto questionnaireDto = questionnaireService.getQuestionnaire();
        log.info("Questionnaire successfully fetched!");
        return new ResponseEntity<>(questionnaireDto, HttpStatus.OK);
    }

    @PostMapping("/answers")
    public ResponseEntity<Void> postAnswers(@RequestBody List<AnswerDto> answerDtoList) {

        log.info("Saving answers...");
        questionnaireService.saveAllAnswers(answerDtoList);
        log.info("Answers successfully saved!");
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping("answer-statistics/{questionId}")
    public ResponseEntity<QuestionDto> getAnswersByQuestion(@PathVariable Long questionId) {

        log.info("Loading answers for question with id {}", questionId);
        QuestionDto questionDto = questionnaireService.getAnswersByQuestion(questionId);
        log.info("Answers for question with id {} successfully fetched!", questionId);
        return new ResponseEntity<>(questionDto, HttpStatus.OK);
    }
}

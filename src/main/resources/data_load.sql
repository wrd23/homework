INSERT INTO question (id, text, answer_type, answers)
    VALUES (1, 'Which javascript framework do you like the most?', 'SELECT_ONE', ['Angular', 'React', 'Vue', 'Svelte']);
INSERT INTO question (id, text, answer_type, answers)
    VALUES (2, 'Which IDEs do you like to work with?', 'SELECT_MORE', ['Webstorm', 'IntelliJ IDEA', 'VSCode', 'Sublime']);
INSERT INTO question (id, text, answer_type, answers)
    VALUES (3, 'What''s your proudest moment so far?', 'FREE_TEXT');
